const express = require("express");
const app = express();
// Require the necessary discord.js classes
const { Client, Events, GatewayIntentBits } = require('discord.js');
const { token } = require('./config.json');

app.use(function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*");
  const allowedOrigins = ['http://localhost:8100', 'https://limoncello-syndicate.netlify.app', 'http://127.0.0.1:5500', 'https://limoncello-syndicate.netlify.app/join', 'http://127.0.0.1:5500/join.html'];
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-credentials", true);
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, UPDATE");
  next();
});

const port = process.env.PORT || 5000;

// Create a new client instance
const client = new Client({ intents: [GatewayIntentBits.Guilds] });

// When the client is ready, run this code (only once).
// The distinction between `client: Client<boolean>` and `readyClient: Client<true>` is important for TypeScript developers.
// It makes some properties non-nullable.
client.once(Events.ClientReady, readyClient => {
	console.log(`Ready! Logged in as ${readyClient.user.tag}`);
});

// Log in to Discord with your client's token
client.login(token);

app.post("/", (req, res) => {
  let username = req.query.username;
  let channel = client.channels.cache.find(channel => channel.id === "1216627621991940147")
  channel.send("@" + username + " begs for refuge. The choice is yours, lemon.");
  
  return res.status(200).send({
    message: "Data Received!",
  });
})

app.listen(port, () => {
  console.log("Listening on " + port);
});

module.exports = app;
